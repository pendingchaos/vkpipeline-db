/*
 * Copyright © 2018 Valve Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef SERIALIZE_H
#define SERIALIZE_H

#include <vulkan/vulkan.h>

#include "blob.h"

#ifdef __cplusplus
extern "C" {
#endif

struct pipeline_info {
    /* Graphics or compute pipeline. */
    VkPipelineBindPoint bindPoint;

    /* Shader stages and modules. */
    uint32_t stageCount;
    VkPipelineShaderStageCreateInfo *pShaderStagesInfo;
    VkShaderModuleCreateInfo *pShaderModulesInfo;

    /* Descriptor set layouts. */
    VkDescriptorSetLayoutCreateInfo *pSetLayoutsInfo;

    /* Graphics states. */
    VkPipelineVertexInputStateCreateInfo vertexInputState;
    VkPipelineInputAssemblyStateCreateInfo inputAssemblyState;
    VkPipelineTessellationStateCreateInfo tessellationState;
    VkPipelineViewportStateCreateInfo viewportState;
    VkPipelineRasterizationStateCreateInfo rasterizationState;
    VkPipelineMultisampleStateCreateInfo multisampleState;
    VkPipelineDepthStencilStateCreateInfo depthStencilState;
    VkPipelineColorBlendStateCreateInfo colorBlendState;
    VkPipelineDynamicStateCreateInfo dynamicState;

    /* Pipeline layout and render pass. */
    VkPipelineLayoutCreateInfo pipelineLayoutInfo;
    VkRenderPassCreateInfo renderPassInfo;
};

void
serialize_pipeline(struct pipeline_info *pipeline,
                   struct blob *metadata);

bool
deserialize_pipeline(struct pipeline_info *pipeline,
                     struct blob_reader *metadata);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
